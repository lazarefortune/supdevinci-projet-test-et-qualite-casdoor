const url = require("url");
const { SDK } = require("casdoor-nodejs-sdk");
const express = require("express");
const fs = require("fs");
const path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");

//init sdk
const cert = `-----BEGIN CERTIFICATE-----
MIIE3TCCAsWgAwIBAgIDAeJAMA0GCSqGSIb3DQEBCwUAMCgxDjAMBgNVBAoTBWFk
bWluMRYwFAYDVQQDEw1jZXJ0LWJ1aWx0LWluMB4XDTI0MDEyMzA5MDMxMloXDTQ0
MDEyMzA5MDMxMlowKDEOMAwGA1UEChMFYWRtaW4xFjAUBgNVBAMTDWNlcnQtYnVp
bHQtaW4wggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC7AP6u+LWIZKAy
uy6LeEWb7H/zKWHWg9E1qDLafXvvfeKH0MdIu9trf2iWxNKu6OK72IV+At9ktXKb
LMV4Q2SzFaRbZSieDBKlI2iJaKtz5Gt6fmtD0wg/frUsZSJeKZpOe9v4MuSNpa1U
GQEaLAnazHYyIKIR3pgYRhlGzUYbEMrZCNY1Ys72+17Ksx82vi8Pfj/IqasRI1tk
g+cSry7fspHipsYSC+iaOs1yqr54QNTE3tJt8ImlywTl8wySJLNvj9wVpwF6vgbI
KqtccJyzd4GVjlvMl3ZQxKxXFz7yc4Ow7S2GtzV6q1RXsOIRQgmdRDEg03fEp1nH
1Cnxum4f1fVMfTSztzubVTH7/S0vo5XUujbyQfeIn3/zdWsFm2Q3GJmFe8AxBGgt
r3F5BhK/3mrjbspLLv+rikgwemIcnGfEX3ORZPlbX2ZeQHbN0rHvcV+Br05FssKx
uRWXOV1gSdRAwm1c5iFYTrVeFM4zgImuOry7Gge1G8yK1nf8POqYVjlj5n9Wj3vc
JnLGMkjK5Sr5wVnglRgz84niCpseSfBQU0o3R43MhkaxP2FYzkXR2DinFB0k0a+G
UI103jJ3p7qk93Qj8Jd08HEltJb7FArHlKtsoLMYLnFP3gUA2O1CiGSWsc/aWNvR
d3S7caVC5RyJ8BY2KpGJ7x81JeF0wwIDAQABoxAwDjAMBgNVHRMBAf8EAjAAMA0G
CSqGSIb3DQEBCwUAA4ICAQCd7aOEzIQlghtuq547VL+uKTQf8hAOyJctAScz7WuN
ZiHZmY/ArTdnjFZQY4wTmli5pwEd3yKy+KVXqVTaMeA81Uo7cUfHv1mGwDtwmNBf
h6maA23c2hbjspSi3P2sWr4nw7ptwtzvep8xvfO3sGDF4ZxIlZyzyDZnftz7NVNw
hMn+cPaTchBqmVSxz5KOasDb+S88Cr8wNG0J3BfpuYPOMohyTdNeFDUSF4kV22gW
tDMuUZpf6x1OBNvtKIeX2ouP6+o+1R9cjtyHSy39qe4kEOkd+MJYG7c+NfALTPOG
jrkDHhZD9MWSPtJH9eLiFownCnfaPBHCwrjYTuJNpTuOe/iALZOXMdqUENBIBGRn
B52SOQgIe4MIZ1PE/g5fn7s7xs5JWnysTOhGlgRtq91tBj6tjUH7Dcs7T+QD/rn9
FoDB3gvMmpZG8mv7wtVMyPmrjrrEjDWK5Gums5CTK19BTkly7ekpQ5s4z2o02Iyo
hCnORhaNdfqgnzsDGGRKUzKNaN4YPWkJuZ1iECQPEJ5U2KizsWNJssTvH/DoYCKM
RPRuX4fS12mDFvNUrd8/WbBGHH16GUca8VsRfgbusYamw9ZDGIVGiF040WV0D45g
o18qDL8D/NjW4HAogLGH0jBG2tOKOq4RjeOnjRVTQxee2CDykz3bCAUWfrCzbfAf
9Q==
-----END CERTIFICATE-----
`;

const authCfg = {
  endpoint: "http://localhost:8000",
  clientId: "1ae00f925722500e1b39",
  clientSecret: "a736be7b73a9e56a893b2de07c2d3b4dcf2d491c",
  certificate: cert,
  orgName: "built-in",
  appName: "appvue",
};

const sdk = new SDK(authCfg);
const app = express();
app.use(
  cors({
    origin: "http://localhost:5173",
    credentials: true,
  }),
);
app.use(bodyParser.json());

app.get("/", (req, res) => {
  fs.readFile(path.resolve(__dirname, "../index.html"), (err, data) => {
    res.setHeader("Content-Type", "text/html");
    res.send(data);
  });
});

app.post("/api/signin", (req, res) => {
  const urlObj = url.parse(req.url, true).query;
  sdk.getAuthToken(urlObj.code).then((response) => {
    const accessToken = response.access_token;
    // const refresh_token = response.refresh_token;
    res.send(JSON.stringify({ token: accessToken }));
  });
});

app.get("/api/getUserInfo", (req, res) => {
  const urlObj = url.parse(req.url, true).query;
  const user = sdk.parseJwtToken(urlObj.token);
  res.send(JSON.stringify(user));
});

app.get("/api/get-users", (req, res) => {
  sdk.getUsers().then((response) => {
    res.send(response.data);
  });
});

app.post("/api/delete-user", (req, res) => {
  sdk.deleteUser(req.body).then((response) => {
    res.send(response.data);
  });
});

app.post("/api/add-user", (req, res) => {
  sdk.addUser(req.body).then((response) => {
    res.send(response.data);
  });
});

app.post("/api/update-user", (req, res) => {
  sdk.updateUser(req.body).then((response) => {
    res.send(response.data);
  });
});

app.get("/api/get-permissions", (req, res) => {
  sdk.getPermissions().then((response) => {
    res.send(response.data);
  });
});

app.post("/api/update-permission", (req, res) => {
  sdk.updatePermission(req.body).then((response) => {
    res.send(response.data);
  });
});

app.listen(8080, () => {});
