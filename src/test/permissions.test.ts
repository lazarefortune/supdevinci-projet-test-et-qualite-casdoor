import { getPermissions } from "../utils";

global.fetch = jest.fn();

describe("getPermissions", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should return permissions data on successful fetch", async () => {
    const mockData = { data: [{ id: 1, name: "Permission1" }] };
    (global.fetch as jest.Mock).mockResolvedValue({
      json: () => Promise.resolve(mockData),
    });
    const result = await getPermissions();
    expect(global.fetch).toHaveBeenCalledWith(
      "http://localhost:8080/api/get-permissions",
    );
    expect(result).toEqual(mockData.data);
  });

  it("should handle fetch failure", async () => {
    (global.fetch as jest.Mock).mockRejectedValue(new Error("Network error"));
    await expect(getPermissions()).rejects.toThrow("Network error");
  });
});
