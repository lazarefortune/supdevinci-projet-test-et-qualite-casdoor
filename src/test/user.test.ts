import { getUsers } from "../utils";

global.fetch = jest.fn();

describe("getUsers", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should return user data on successful fetch", async () => {
    const mockData = { data: [{ id: 1, name: "User1" }] };
    (global.fetch as jest.Mock).mockResolvedValue({
      json: () => Promise.resolve(mockData),
    });

    const result = await getUsers();

    expect(global.fetch).toHaveBeenCalledWith(
      "http://localhost:8080/api/get-users",
    );
    expect(result).toEqual(mockData.data);
  });

  it("should handle fetch failure", async () => {
    (global.fetch as jest.Mock).mockRejectedValue(new Error("Network error"));
    await expect(getUsers()).rejects.toThrow("Network error");
  });
});
