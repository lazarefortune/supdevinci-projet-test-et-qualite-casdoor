import { createApp } from "vue";
import App from "./App.vue";
import "./style.css";
import CasdoorProvider from "casdoor-vue-sdk";
import router from "./router/index";
import PrimeVue from "primevue/config";
import "primevue/resources/themes/lara-dark-green/theme.css";
import "primeicons/primeicons.css";
import ToastService from "primevue/toastservice";
const casdoorConfig = {
  serverUrl: import.meta.env.VITE_SERVER_URL,
  clientId: import.meta.env.VITE_CLIENT_ID,
  organizationName: import.meta.env.VITE_ORGANIZATION_NAME,
  appName: import.meta.env.VITE_APP_NAME,
  redirectPath: import.meta.env.VITE_REDIRECT_PATH,
};

const app = createApp(App);
app.use(router);
app.use(PrimeVue);
app.use(ToastService);
app.use(CasdoorProvider, casdoorConfig);
app.mount("body");
