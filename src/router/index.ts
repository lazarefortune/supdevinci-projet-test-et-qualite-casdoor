import * as VueRouter from "vue-router";
import HomePage from "../views/HomePage.vue";
import DashboardPage from "../views/DashboardPage.vue";
import UsersVue from "../views/UsersPage.vue";
import CallbackPageVue from "../views/CallbackPage.vue";

const routes = [
  { path: "/", name: "HomePage", component: HomePage },
  { path: "/Dashboard", name: "DashboardPage", component: DashboardPage },
  { path: "/users", name: "Users", component: UsersVue },
  { path: "/callback", name: "Callback", component: CallbackPageVue },
];

const router = VueRouter.createRouter({
  history: VueRouter.createWebHistory(),
  routes,
});

export default router;
