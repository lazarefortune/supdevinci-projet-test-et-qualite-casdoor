import { reactive } from "vue";
import { ObjectUser } from "../Api";

const store = reactive<{
  user: ObjectUser | null;
}>({
  user: null,
});

export default store;
