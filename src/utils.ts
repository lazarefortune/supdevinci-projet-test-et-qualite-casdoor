import { ObjectUser } from "./Api";

export async function getPermissions() {
  const res = await fetch("http://localhost:8080/api/get-permissions");
  const data = await res.json();
  return data.data;
}

export async function getUsers(): Promise<ObjectUser[]> {
  const res = await fetch("http://localhost:8080/api/get-users");
  const data = await res.json();
  return data.data;
}
