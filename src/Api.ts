/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/** Enforcer */
export type CasbinEnforcer = object;

/** AuthForm */
export type ControllersAuthForm = object;

/** EmailForm */
export interface ControllersEmailForm {
  content?: string;
  provider?: string;
  receivers?: string[];
  sender?: string;
  title?: string;
}

/** LaravelResponse */
export interface ControllersLaravelResponse {
  created_at?: string;
  email?: string;
  email_verified_at?: string;
  id?: string;
  name?: string;
  updated_at?: string;
}

/** LdapResp */
export interface ControllersLdapResp {
  existUuids?: string[];
  users?: ObjectLdapUser[];
}

/** LdapSyncResp */
export interface ControllersLdapSyncResp {
  exist?: ObjectLdapUser[];
  failed?: ObjectLdapUser[];
}

/** NotificationForm */
export interface ControllersNotificationForm {
  content?: string;
}

/** Response */
export interface ControllersResponse {
  data?: any;
  data2?: any;
  msg?: string;
  name?: string;
  status?: string;
  sub?: string;
}

/** SmsForm */
export interface ControllersSmsForm {
  content?: string;
  organizationId?: string;
  receivers?: string[];
}

/** JSONWebKey */
export type JoseJSONWebKey = object;

/** Model */
export type ModelModel = object;

/** object */
export type Object = object;

/** AccountItem */
export interface ObjectAccountItem {
  modifyRule?: string;
  name?: string;
  viewRule?: string;
  visible?: boolean;
}

/** Adapter */
export interface ObjectAdapter {
  createdTime?: string;
  database?: string;
  databaseType?: string;
  host?: string;
  name?: string;
  owner?: string;
  password?: string;
  /** @format int64 */
  port?: number;
  table?: string;
  type?: string;
  useSameDb?: boolean;
  user?: string;
}

/** Application */
export interface ObjectApplication {
  affiliationUrl?: string;
  cert?: string;
  certPublicKey?: string;
  clientId?: string;
  clientSecret?: string;
  createdTime?: string;
  description?: string;
  displayName?: string;
  enableAutoSignin?: boolean;
  enableCodeSignin?: boolean;
  enableLinkWithEmail?: boolean;
  enablePassword?: boolean;
  enableSamlC14n10?: boolean;
  enableSamlCompress?: boolean;
  enableSignUp?: boolean;
  enableSigninSession?: boolean;
  enableWebAuthn?: boolean;
  /** @format int64 */
  expireInHours?: number;
  /** @format int64 */
  failedSigninFrozenTime?: number;
  /** @format int64 */
  failedSigninLimit?: number;
  forgetUrl?: string;
  formBackgroundUrl?: string;
  formCss?: string;
  formCssMobile?: string;
  /** @format int64 */
  formOffset?: number;
  formSideHtml?: string;
  grantTypes?: string[];
  homepageUrl?: string;
  invitationCodes?: string[];
  logo?: string;
  name?: string;
  orgChoiceMode?: string;
  organization?: string;
  organizationObj?: ObjectOrganization;
  owner?: string;
  providers?: ObjectProviderItem[];
  redirectUris?: string[];
  /** @format int64 */
  refreshExpireInHours?: number;
  samlAttributes?: ObjectSamlItem[];
  samlReplyUrl?: string;
  signinHtml?: string;
  signinMethods?: ObjectSigninMethod[];
  signinUrl?: string;
  signupHtml?: string;
  signupItems?: ObjectSignupItem[];
  signupUrl?: string;
  tags?: string[];
  termsOfUse?: string;
  themeData?: ObjectThemeData;
  tokenFields?: string[];
  tokenFormat?: string;
}

/** Cert */
export interface ObjectCert {
  /** @format int64 */
  bitSize?: number;
  certificate?: string;
  createdTime?: string;
  cryptoAlgorithm?: string;
  displayName?: string;
  /** @format int64 */
  expireInYears?: number;
  name?: string;
  owner?: string;
  privateKey?: string;
  scope?: string;
  type?: string;
}

/** Enforcer */
export interface ObjectEnforcer {
  adapter?: string;
  createdTime?: string;
  description?: string;
  displayName?: string;
  model?: string;
  modelCfg?: any;
  name?: string;
  owner?: string;
  updatedTime?: string;
}

/** GaugeVecInfo */
export interface ObjectGaugeVecInfo {
  method?: string;
  name?: string;
  /** @format double */
  throughput?: number;
}

/** Group */
export interface ObjectGroup {
  children?: ObjectGroup[];
  contactEmail?: string;
  createdTime?: string;
  displayName?: string;
  isEnabled?: boolean;
  isTopGroup?: boolean;
  key?: string;
  manager?: string;
  name?: string;
  owner?: string;
  parentId?: string;
  title?: string;
  type?: string;
  updatedTime?: string;
  users?: ObjectUser[];
}

/** Header */
export interface ObjectHeader {
  name?: string;
  value?: string;
}

/** HistogramVecInfo */
export interface ObjectHistogramVecInfo {
  /** @format int64 */
  count?: number;
  latency?: string;
  method?: string;
  name?: string;
}

/** IntrospectionResponse */
export interface ObjectIntrospectionResponse {
  active?: boolean;
  aud?: string[];
  client_id?: string;
  /** @format int64 */
  exp?: number;
  /** @format int64 */
  iat?: number;
  iss?: string;
  jti?: string;
  /** @format int64 */
  nbf?: number;
  scope?: string;
  sub?: string;
  token_type?: string;
  username?: string;
}

/** Invitation */
export interface ObjectInvitation {
  application?: string;
  code?: string;
  createdTime?: string;
  displayName?: string;
  email?: string;
  name?: string;
  owner?: string;
  phone?: string;
  /** @format int64 */
  quota?: number;
  signupGroup?: string;
  state?: string;
  updatedTime?: string;
  /** @format int64 */
  usedCount?: number;
  username?: string;
}

/** Ldap */
export interface ObjectLdap {
  /** @format int64 */
  autoSync?: number;
  baseDn?: string;
  createdTime?: string;
  enableSsl?: boolean;
  filter?: string;
  filterFields?: string[];
  host?: string;
  id?: string;
  lastSync?: string;
  owner?: string;
  password?: string;
  /** @format int64 */
  port?: number;
  serverName?: string;
  username?: string;
}

/** LdapUser */
export interface ObjectLdapUser {
  EmailAddress?: string;
  Mail?: string;
  MobileTelephoneNumber?: string;
  PostalAddress?: string;
  RegisteredAddress?: string;
  TelephoneNumber?: string;
  address?: string;
  cn?: string;
  displayName?: string;
  email?: string;
  gidNumber?: string;
  groupId?: string;
  memberOf?: string;
  mobile?: string;
  uid?: string;
  uidNumber?: string;
  userPrincipalName?: string;
  uuid?: string;
}

/** ManagedAccount */
export interface ObjectManagedAccount {
  application?: string;
  password?: string;
  signinUrl?: string;
  username?: string;
}

/** MfaItem */
export interface ObjectMfaItem {
  name?: string;
  rule?: string;
}

/** MfaProps */
export interface ObjectMfaProps {
  countryCode?: string;
  enabled?: boolean;
  isPreferred?: boolean;
  mfaType?: string;
  recoveryCodes?: string[];
  secret?: string;
  url?: string;
}

/** Model */
export interface ObjectModel {
  createdTime?: string;
  description?: string;
  displayName?: string;
  modelText?: string;
  name?: string;
  owner?: string;
}

/** OidcDiscovery */
export interface ObjectOidcDiscovery {
  authorization_endpoint?: string;
  claims_supported?: string[];
  end_session_endpoint?: string;
  grant_types_supported?: string[];
  id_token_signing_alg_values_supported?: string[];
  introspection_endpoint?: string;
  issuer?: string;
  jwks_uri?: string;
  request_object_signing_alg_values_supported?: string[];
  request_parameter_supported?: boolean;
  response_modes_supported?: string[];
  response_types_supported?: string[];
  scopes_supported?: string[];
  subject_types_supported?: string[];
  token_endpoint?: string;
  userinfo_endpoint?: string;
}

/** Organization */
export interface ObjectOrganization {
  accountItems?: ObjectAccountItem[];
  countryCodes?: string[];
  createdTime?: string;
  defaultApplication?: string;
  defaultAvatar?: string;
  defaultPassword?: string;
  displayName?: string;
  enableSoftDeletion?: boolean;
  favicon?: string;
  /** @format int64 */
  initScore?: number;
  isProfilePublic?: boolean;
  languages?: string[];
  masterPassword?: string;
  masterVerificationCode?: string;
  mfaItems?: ObjectMfaItem[];
  name?: string;
  owner?: string;
  passwordOptions?: string[];
  passwordSalt?: string;
  passwordType?: string;
  tags?: string[];
  themeData?: ObjectThemeData;
  websiteUrl?: string;
}

/** Ormer */
export interface ObjectOrmer {
  Engine?: XormEngine;
  dataSourceName?: string;
  dbName?: string;
  driverName?: string;
}

/** Payment */
export interface ObjectPayment {
  createdTime?: string;
  currency?: string;
  detail?: string;
  displayName?: string;
  invoiceRemark?: string;
  invoiceTaxId?: string;
  invoiceTitle?: string;
  invoiceType?: string;
  invoiceUrl?: string;
  message?: string;
  name?: string;
  outOrderId?: string;
  owner?: string;
  payUrl?: string;
  personEmail?: string;
  personIdCard?: string;
  personName?: string;
  personPhone?: string;
  /** @format double */
  price?: number;
  productDisplayName?: string;
  productName?: string;
  provider?: string;
  returnUrl?: string;
  state?: PpPaymentState;
  successUrl?: string;
  tag?: string;
  type?: string;
  user?: string;
}

/** Permission */
export interface ObjectPermission {
  actions?: string[];
  adapter?: string;
  approveTime?: string;
  approver?: string;
  createdTime?: string;
  description?: string;
  displayName?: string;
  domains?: string[];
  effect?: string;
  groups?: string[];
  isEnabled?: boolean;
  model?: string;
  name?: string;
  owner?: string;
  resourceType?: string;
  resources?: string[];
  roles?: string[];
  state?: string;
  submitter?: string;
  users?: string[];
}

/** Plan */
export interface ObjectPlan {
  createdTime?: string;
  currency?: string;
  description?: string;
  displayName?: string;
  isEnabled?: boolean;
  name?: string;
  options?: string[];
  owner?: string;
  paymentProviders?: string[];
  period?: string;
  /** @format double */
  price?: number;
  product?: string;
  role?: string;
}

/** Pricing */
export interface ObjectPricing {
  application?: string;
  createdTime?: string;
  description?: string;
  displayName?: string;
  isEnabled?: boolean;
  name?: string;
  owner?: string;
  plans?: string[];
  /** @format int64 */
  trialDuration?: number;
}

/** Product */
export interface ObjectProduct {
  createdTime?: string;
  currency?: string;
  description?: string;
  detail?: string;
  displayName?: string;
  image?: string;
  name?: string;
  owner?: string;
  /** @format double */
  price?: number;
  providerObjs?: ObjectProvider[];
  providers?: string[];
  /** @format int64 */
  quantity?: number;
  returnUrl?: string;
  /** @format int64 */
  sold?: number;
  state?: string;
  tag?: string;
}

/** PrometheusInfo */
export interface ObjectPrometheusInfo {
  apiLatency?: ObjectHistogramVecInfo[];
  apiThroughput?: ObjectGaugeVecInfo[];
  /** @format double */
  totalThroughput?: number;
}

/** Provider */
export interface ObjectProvider {
  appId?: string;
  bucket?: string;
  category?: string;
  cert?: string;
  clientId?: string;
  clientId2?: string;
  clientSecret?: string;
  clientSecret2?: string;
  content?: string;
  createdTime?: string;
  customAuthUrl?: string;
  customLogo?: string;
  customTokenUrl?: string;
  customUserInfoUrl?: string;
  disableSsl?: boolean;
  displayName?: string;
  domain?: string;
  enableSignAuthnRequest?: boolean;
  endpoint?: string;
  host?: string;
  idP?: string;
  intranetEndpoint?: string;
  issuerUrl?: string;
  metadata?: string;
  method?: string;
  name?: string;
  owner?: string;
  pathPrefix?: string;
  /** @format int64 */
  port?: number;
  providerUrl?: string;
  receiver?: string;
  regionId?: string;
  scopes?: string;
  signName?: string;
  subType?: string;
  templateCode?: string;
  title?: string;
  type?: string;
  userMapping?: any;
}

/** ProviderItem */
export interface ObjectProviderItem {
  canSignIn?: boolean;
  canSignUp?: boolean;
  canUnlink?: boolean;
  name?: string;
  owner?: string;
  prompted?: boolean;
  provider?: ObjectProvider;
  rule?: string;
  signupGroup?: string;
}

/** Resource */
export interface ObjectResource {
  application?: string;
  createdTime?: string;
  description?: string;
  fileFormat?: string;
  fileName?: string;
  /** @format int64 */
  fileSize?: number;
  fileType?: string;
  name?: string;
  owner?: string;
  parent?: string;
  provider?: string;
  tag?: string;
  url?: string;
  user?: string;
}

/** Role */
export interface ObjectRole {
  createdTime?: string;
  description?: string;
  displayName?: string;
  domains?: string[];
  groups?: string[];
  isEnabled?: boolean;
  name?: string;
  owner?: string;
  roles?: string[];
  users?: string[];
}

/** SamlItem */
export interface ObjectSamlItem {
  name?: string;
  nameFormat?: string;
  value?: string;
}

/** SigninMethod */
export interface ObjectSigninMethod {
  displayName?: string;
  name?: string;
  rule?: string;
}

/** SignupItem */
export interface ObjectSignupItem {
  label?: string;
  name?: string;
  placeholder?: string;
  prompted?: boolean;
  regex?: string;
  required?: boolean;
  rule?: string;
  visible?: boolean;
}

/** Subscription */
export interface ObjectSubscription {
  createdTime?: string;
  description?: string;
  displayName?: string;
  /** @format datetime */
  endTime?: string;
  name?: string;
  owner?: string;
  payment?: string;
  period?: string;
  plan?: string;
  pricing?: string;
  /** @format datetime */
  startTime?: string;
  state?: ObjectSubscriptionState;
  user?: string;
}

/**
 * SubscriptionState
 * @example "Pending"
 */
export enum ObjectSubscriptionState {
  SubStatePendingPending = "SubStatePending = Pending",
  SubStateErrorError = "SubStateError = Error",
  SubStateSuspendedSuspended = "SubStateSuspended = Suspended",
  SubStateActiveActive = "SubStateActive = Active",
  SubStateUpcomingUpcoming = "SubStateUpcoming = Upcoming",
  SubStateExpiredExpired = "SubStateExpired = Expired",
}

/** Syncer */
export interface ObjectSyncer {
  affiliationTable?: string;
  avatarBaseUrl?: string;
  createdTime?: string;
  database?: string;
  databaseType?: string;
  errorText?: string;
  host?: string;
  isEnabled?: boolean;
  isReadOnly?: boolean;
  name?: string;
  organization?: string;
  owner?: string;
  password?: string;
  /** @format int64 */
  port?: number;
  sslMode?: string;
  /** @format int64 */
  syncInterval?: number;
  table?: string;
  tableColumns?: ObjectTableColumn[];
  type?: string;
  user?: string;
}

/** TableColumn */
export interface ObjectTableColumn {
  casdoorName?: string;
  isHashed?: boolean;
  isKey?: boolean;
  name?: string;
  type?: string;
  values?: string[];
}

/** ThemeData */
export interface ObjectThemeData {
  /** @format int64 */
  borderRadius?: number;
  colorPrimary?: string;
  isCompact?: boolean;
  isEnabled?: boolean;
  themeType?: string;
}

/** Token */
export interface ObjectToken {
  accessToken?: string;
  accessTokenHash?: string;
  application?: string;
  code?: string;
  codeChallenge?: string;
  /** @format int64 */
  codeExpireIn?: number;
  codeIsUsed?: boolean;
  createdTime?: string;
  /** @format int64 */
  expiresIn?: number;
  name?: string;
  organization?: string;
  owner?: string;
  refreshToken?: string;
  refreshTokenHash?: string;
  scope?: string;
  tokenType?: string;
  user?: string;
}

/** TokenError */
export interface ObjectTokenError {
  error?: string;
  error_description?: string;
}

/** TokenWrapper */
export interface ObjectTokenWrapper {
  access_token?: string;
  /** @format int64 */
  expires_in?: number;
  id_token?: string;
  refresh_token?: string;
  scope?: string;
  token_type?: string;
}

/** User */
export interface ObjectUser {
  accessKey?: string;
  accessSecret?: string;
  address?: string[];
  adfs?: string;
  affiliation?: string;
  alipay?: string;
  amazon?: string;
  apple?: string;
  auth0?: string;
  avatar?: string;
  avatarType?: string;
  azuread?: string;
  azureadb2c?: string;
  baidu?: string;
  battlenet?: string;
  bilibili?: string;
  bio?: string;
  birthday?: string;
  bitbucket?: string;
  box?: string;
  casdoor?: string;
  cloudfoundry?: string;
  countryCode?: string;
  createdIp?: string;
  createdTime?: string;
  custom?: string;
  dailymotion?: string;
  deezer?: string;
  digitalocean?: string;
  dingtalk?: string;
  discord?: string;
  displayName?: string;
  douyin?: string;
  dropbox?: string;
  education?: string;
  email?: string;
  emailVerified?: boolean;
  eveonline?: string;
  externalId?: string;
  facebook?: string;
  firstName?: string;
  fitbit?: string;
  gender?: string;
  gitea?: string;
  gitee?: string;
  github?: string;
  gitlab?: string;
  google?: string;
  groups?: string[];
  hash?: string;
  heroku?: string;
  homepage?: string;
  id?: string;
  idCard?: string;
  idCardType?: string;
  influxcloud?: string;
  infoflow?: string;
  instagram?: string;
  intercom?: string;
  isAdmin?: boolean;
  isDefaultAvatar?: boolean;
  isDeleted?: boolean;
  isForbidden?: boolean;
  isOnline?: boolean;
  kakao?: string;
  /** @format int64 */
  karma?: number;
  language?: string;
  lark?: string;
  lastName?: string;
  lastSigninIp?: string;
  lastSigninTime?: string;
  lastSigninWrongTime?: string;
  lastfm?: string;
  ldap?: string;
  line?: string;
  linkedin?: string;
  location?: string;
  mailru?: string;
  managedAccounts?: ObjectManagedAccount[];
  meetup?: string;
  metamask?: string;
  mfaEmailEnabled?: boolean;
  mfaPhoneEnabled?: boolean;
  microsoftonline?: string;
  multiFactorAuths?: ObjectMfaProps[];
  name?: string;
  naver?: string;
  nextcloud?: string;
  okta?: string;
  onedrive?: string;
  oura?: string;
  owner?: string;
  password?: string;
  passwordSalt?: string;
  passwordType?: string;
  patreon?: string;
  paypal?: string;
  permanentAvatar?: string;
  permissions?: ObjectPermission[];
  phone?: string;
  preHash?: string;
  preferredMfaType?: string;
  properties?: any;
  qq?: string;
  /** @format int64 */
  ranking?: number;
  recoveryCodes?: string[];
  region?: string;
  roles?: ObjectRole[];
  salesforce?: string;
  /** @format int64 */
  score?: number;
  shopify?: string;
  /** @format int64 */
  signinWrongTimes?: number;
  signupApplication?: string;
  slack?: string;
  soundcloud?: string;
  spotify?: string;
  steam?: string;
  strava?: string;
  stripe?: string;
  tag?: string;
  tiktok?: string;
  title?: string;
  totpSecret?: string;
  tumblr?: string;
  twitch?: string;
  twitter?: string;
  type?: string;
  typetalk?: string;
  uber?: string;
  updatedTime?: string;
  vk?: string;
  web3onboard?: string;
  webauthnCredentials?: WebauthnCredential[];
  wechat?: string;
  wecom?: string;
  weibo?: string;
  wepay?: string;
  xero?: string;
  yahoo?: string;
  yammer?: string;
  yandex?: string;
  zoom?: string;
}

/** Userinfo */
export interface ObjectUserinfo {
  address?: string;
  aud?: string;
  email?: string;
  email_verified?: boolean;
  groups?: string[];
  iss?: string;
  name?: string;
  phone?: string;
  picture?: string;
  preferred_username?: string;
  sub?: string;
}

/** Webhook */
export interface ObjectWebhook {
  contentType?: string;
  createdTime?: string;
  events?: string[];
  headers?: ObjectHeader[];
  isEnabled?: boolean;
  isUserExtended?: boolean;
  method?: string;
  name?: string;
  organization?: string;
  owner?: string;
  url?: string;
}

/**
 * PaymentState
 * @example "Paid"
 */
export enum PpPaymentState {
  PaymentStatePaidPaid = "PaymentStatePaid = Paid",
  PaymentStateCreatedCreated = "PaymentStateCreated = Created",
  PaymentStateCanceledCanceled = "PaymentStateCanceled = Canceled",
  PaymentStateTimeoutTimeout = "PaymentStateTimeout = Timeout",
  PaymentStateErrorError = "PaymentStateError = Error",
}

/** CredentialAssertion */
export type ProtocolCredentialAssertion = object;

/** CredentialAssertionResponse */
export type ProtocolCredentialAssertionResponse = object;

/** CredentialCreation */
export type ProtocolCredentialCreation = object;

/** CredentialCreationResponse */
export type ProtocolCredentialCreationResponse = object;

/** SystemInfo */
export interface UtilSystemInfo {
  cpuUsage?: number[];
  /** @format int64 */
  memoryTotal?: number;
  /** @format int64 */
  memoryUsed?: number;
}

/** VersionInfo */
export interface UtilVersionInfo {
  commitId?: string;
  /** @format int64 */
  commitOffset?: number;
  version?: string;
}

/** Credential */
export type WebauthnCredential = object;

/** Engine */
export type XormEngine = object;

/** Adapter */
export type XormadapterAdapter = object;

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;

export interface FullRequestParams extends Omit<RequestInit, "body"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
  securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
  Text = "text/plain",
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = "/";
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: "same-origin",
    headers: {},
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === "number" ? value : `${value}`)}`;
  }

  protected addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  protected addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join("&");
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => "undefined" !== typeof query[key]);
    return keys
      .map((key) => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
      .join("&");
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : "";
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === "object" || typeof input === "string") ? JSON.stringify(input) : input,
    [ContentType.Text]: (input: any) => (input !== null && typeof input !== "string" ? JSON.stringify(input) : input),
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === "object" && property !== null
            ? JSON.stringify(property)
            : `${property}`,
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  protected mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  protected createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(`${baseUrl || this.baseUrl || ""}${path}${queryString ? `?${queryString}` : ""}`, {
      ...requestParams,
      headers: {
        ...(requestParams.headers || {}),
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
      },
      signal: (cancelToken ? this.createAbortSignal(cancelToken) : requestParams.signal) || null,
      body: typeof body === "undefined" || body === null ? null : payloadFormatter(body),
    }).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title Casdoor RESTful API
 * @version 1.503.0
 * @baseUrl /
 * @contact <casbin@googlegroups.com>
 *
 * Swagger Docs of Casdoor Backend API
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  wellKnown = {
    /**
     * No description
     *
     * @tags OIDC API
     * @name RootControllerGetJwks
     * @request GET:/.well-known/jwks
     */
    rootControllerGetJwks: (params: RequestParams = {}) =>
      this.request<JoseJSONWebKey, any>({
        path: `/.well-known/jwks`,
        method: "GET",
        ...params,
      }),

    /**
     * @description Get Oidc Discovery
     *
     * @tags OIDC API
     * @name RootControllerGetOidcDiscovery
     * @request GET:/.well-known/openid-configuration
     */
    rootControllerGetOidcDiscovery: (params: RequestParams = {}) =>
      this.request<ObjectOidcDiscovery, any>({
        path: `/.well-known/openid-configuration`,
        method: "GET",
        ...params,
      }),
  };
  api = {
    /**
     * @description Get Login Error Counts
     *
     * @tags Callback API
     * @name ApiControllerCallback
     * @request POST:/api/Callback
     */
    apiControllerCallback: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/Callback`,
        method: "POST",
        ...params,
      }),

    /**
     * @description add adapter
     *
     * @tags Adapter API
     * @name ApiControllerAddAdapter
     * @request POST:/api/add-adapter
     */
    apiControllerAddAdapter: (body: ObjectAdapter, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-adapter`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add an application
     *
     * @tags Application API
     * @name ApiControllerAddApplication
     * @request POST:/api/add-application
     */
    apiControllerAddApplication: (body: ObjectApplication, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-application`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add cert
     *
     * @tags Cert API
     * @name ApiControllerAddCert
     * @request POST:/api/add-cert
     */
    apiControllerAddCert: (body: ObjectCert, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-cert`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add enforcer
     *
     * @tags Enforcer API
     * @name ApiControllerAddEnforcer
     * @request POST:/api/add-enforcer
     */
    apiControllerAddEnforcer: (enforcer: Object, params: RequestParams = {}) =>
      this.request<ObjectEnforcer, any>({
        path: `/api/add-enforcer`,
        method: "POST",
        body: enforcer,
        ...params,
      }),

    /**
     * @description add group
     *
     * @tags Group API
     * @name ApiControllerAddGroup
     * @request POST:/api/add-group
     */
    apiControllerAddGroup: (body: ObjectGroup, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-group`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add invitation
     *
     * @tags Invitation API
     * @name ApiControllerAddInvitation
     * @request POST:/api/add-invitation
     */
    apiControllerAddInvitation: (body: ObjectInvitation, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-invitation`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add ldap
     *
     * @tags Account API
     * @name ApiControllerAddLdap
     * @request POST:/api/add-ldap
     */
    apiControllerAddLdap: (body: ObjectLdap, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-ldap`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add model
     *
     * @tags Model API
     * @name ApiControllerAddModel
     * @request POST:/api/add-model
     */
    apiControllerAddModel: (body: ObjectModel, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-model`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add organization
     *
     * @tags Organization API
     * @name ApiControllerAddOrganization
     * @request POST:/api/add-organization
     */
    apiControllerAddOrganization: (body: ObjectOrganization, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-organization`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add payment
     *
     * @tags Payment API
     * @name ApiControllerAddPayment
     * @request POST:/api/add-payment
     */
    apiControllerAddPayment: (body: ObjectPayment, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-payment`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add permission
     *
     * @tags Permission API
     * @name ApiControllerAddPermission
     * @request POST:/api/add-permission
     */
    apiControllerAddPermission: (body: ObjectPermission, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-permission`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add plan
     *
     * @tags Plan API
     * @name ApiControllerAddPlan
     * @request POST:/api/add-plan
     */
    apiControllerAddPlan: (body: ObjectPlan, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-plan`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add pricing
     *
     * @tags Pricing API
     * @name ApiControllerAddPricing
     * @request POST:/api/add-pricing
     */
    apiControllerAddPricing: (body: ObjectPricing, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-pricing`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add product
     *
     * @tags Product API
     * @name ApiControllerAddProduct
     * @request POST:/api/add-product
     */
    apiControllerAddProduct: (body: ObjectProduct, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-product`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add provider
     *
     * @tags Provider API
     * @name ApiControllerAddProvider
     * @request POST:/api/add-provider
     */
    apiControllerAddProvider: (body: ObjectProvider, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-provider`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Resource API
     * @name ApiControllerAddResource
     * @request POST:/api/add-resource
     */
    apiControllerAddResource: (resource: ObjectResource, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-resource`,
        method: "POST",
        body: resource,
        ...params,
      }),

    /**
     * @description add role
     *
     * @tags Role API
     * @name ApiControllerAddRole
     * @request POST:/api/add-role
     */
    apiControllerAddRole: (body: ObjectRole, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-role`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description Add session for one user in one application. If there are other existing sessions, join the session into the list.
     *
     * @tags Session API
     * @name ApiControllerAddSession
     * @request POST:/api/add-session
     */
    apiControllerAddSession: (
      query: {
        /** The id(organization/application/user) of session */
        id: string;
        /** sessionId to be added */
        sessionId: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string[], any>({
        path: `/api/add-session`,
        method: "POST",
        query: query,
        ...params,
      }),

    /**
     * @description add subscription
     *
     * @tags Subscription API
     * @name ApiControllerAddSubscription
     * @request POST:/api/add-subscription
     */
    apiControllerAddSubscription: (body: ObjectSubscription, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-subscription`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add syncer
     *
     * @tags Syncer API
     * @name ApiControllerAddSyncer
     * @request POST:/api/add-syncer
     */
    apiControllerAddSyncer: (body: ObjectSyncer, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-syncer`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add token
     *
     * @tags Token API
     * @name ApiControllerAddToken
     * @request POST:/api/add-token
     */
    apiControllerAddToken: (body: ObjectToken, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-token`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description add user
     *
     * @tags User API
     * @name ApiControllerAddUser
     * @request POST:/api/add-user
     */
    apiControllerAddUser: (body: ObjectUser, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-user`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User API
     * @name ApiControllerAddUserKeys
     * @request POST:/api/add-user-keys
     */
    apiControllerAddUserKeys: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/add-user-keys`,
        method: "POST",
        ...params,
      }),

    /**
     * @description add webhook
     *
     * @tags Webhook API
     * @name ApiControllerAddWebhook
     * @request POST:/api/add-webhook
     */
    apiControllerAddWebhook: (body: ObjectWebhook, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/add-webhook`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description Call Casbin BatchEnforce API
     *
     * @tags Enforcer API
     * @name ApiControllerBatchEnforce
     * @request POST:/api/batch-enforce
     */
    apiControllerBatchEnforce: (
      body: string[],
      query?: {
        /** permission id */
        permissionId?: string;
        /** model id */
        modelId?: string;
        /** owner */
        owner?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/batch-enforce`,
        method: "POST",
        query: query,
        body: body,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description buy product
     *
     * @tags Product API
     * @name ApiControllerBuyProduct
     * @request POST:/api/buy-product
     */
    apiControllerBuyProduct: (
      query: {
        /** The id ( owner/name ) of the product */
        id: string;
        /** The name of the provider */
        providerName: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/buy-product`,
        method: "POST",
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User API
     * @name ApiControllerCheckUserPassword
     * @request POST:/api/check-user-password
     */
    apiControllerCheckUserPassword: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/check-user-password`,
        method: "POST",
        ...params,
      }),

    /**
     * @description delete adapter
     *
     * @tags Adapter API
     * @name ApiControllerDeleteAdapter
     * @request POST:/api/delete-adapter
     */
    apiControllerDeleteAdapter: (body: ObjectAdapter, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-adapter`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete an application
     *
     * @tags Application API
     * @name ApiControllerDeleteApplication
     * @request POST:/api/delete-application
     */
    apiControllerDeleteApplication: (body: ObjectApplication, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-application`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete cert
     *
     * @tags Cert API
     * @name ApiControllerDeleteCert
     * @request POST:/api/delete-cert
     */
    apiControllerDeleteCert: (body: ObjectCert, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-cert`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete enforcer
     *
     * @tags Enforcer API
     * @name ApiControllerDeleteEnforcer
     * @request POST:/api/delete-enforcer
     */
    apiControllerDeleteEnforcer: (body: ObjectEnforcer, params: RequestParams = {}) =>
      this.request<ObjectEnforcer, any>({
        path: `/api/delete-enforcer`,
        method: "POST",
        body: body,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description delete group
     *
     * @tags Group API
     * @name ApiControllerDeleteGroup
     * @request POST:/api/delete-group
     */
    apiControllerDeleteGroup: (body: ObjectGroup, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-group`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete invitation
     *
     * @tags Invitation API
     * @name ApiControllerDeleteInvitation
     * @request POST:/api/delete-invitation
     */
    apiControllerDeleteInvitation: (body: ObjectInvitation, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-invitation`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete ldap
     *
     * @tags Account API
     * @name ApiControllerDeleteLdap
     * @request POST:/api/delete-ldap
     */
    apiControllerDeleteLdap: (body: ObjectLdap, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-ldap`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description : Delete MFA
     *
     * @tags MFA API
     * @name ApiControllerDeleteMfa
     * @request POST:/api/delete-mfa/
     */
    apiControllerDeleteMfa: (params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-mfa/`,
        method: "POST",
        ...params,
      }),

    /**
     * @description delete model
     *
     * @tags Model API
     * @name ApiControllerDeleteModel
     * @request POST:/api/delete-model
     */
    apiControllerDeleteModel: (body: ObjectModel, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-model`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete organization
     *
     * @tags Organization API
     * @name ApiControllerDeleteOrganization
     * @request POST:/api/delete-organization
     */
    apiControllerDeleteOrganization: (body: ObjectOrganization, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-organization`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete payment
     *
     * @tags Payment API
     * @name ApiControllerDeletePayment
     * @request POST:/api/delete-payment
     */
    apiControllerDeletePayment: (body: ObjectPayment, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-payment`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete permission
     *
     * @tags Permission API
     * @name ApiControllerDeletePermission
     * @request POST:/api/delete-permission
     */
    apiControllerDeletePermission: (body: ObjectPermission, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-permission`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete plan
     *
     * @tags Plan API
     * @name ApiControllerDeletePlan
     * @request POST:/api/delete-plan
     */
    apiControllerDeletePlan: (body: ObjectPlan, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-plan`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete pricing
     *
     * @tags Pricing API
     * @name ApiControllerDeletePricing
     * @request POST:/api/delete-pricing
     */
    apiControllerDeletePricing: (body: ObjectPricing, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-pricing`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete product
     *
     * @tags Product API
     * @name ApiControllerDeleteProduct
     * @request POST:/api/delete-product
     */
    apiControllerDeleteProduct: (body: ObjectProduct, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-product`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete provider
     *
     * @tags Provider API
     * @name ApiControllerDeleteProvider
     * @request POST:/api/delete-provider
     */
    apiControllerDeleteProvider: (body: ObjectProvider, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-provider`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Resource API
     * @name ApiControllerDeleteResource
     * @request POST:/api/delete-resource
     */
    apiControllerDeleteResource: (resource: ObjectResource, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-resource`,
        method: "POST",
        body: resource,
        ...params,
      }),

    /**
     * @description delete role
     *
     * @tags Role API
     * @name ApiControllerDeleteRole
     * @request POST:/api/delete-role
     */
    apiControllerDeleteRole: (body: ObjectRole, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-role`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description Delete session for one user in one application.
     *
     * @tags Session API
     * @name ApiControllerDeleteSession
     * @request POST:/api/delete-session
     */
    apiControllerDeleteSession: (
      query: {
        /** The id(organization/application/user) of session */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string[], any>({
        path: `/api/delete-session`,
        method: "POST",
        query: query,
        ...params,
      }),

    /**
     * @description delete subscription
     *
     * @tags Subscription API
     * @name ApiControllerDeleteSubscription
     * @request POST:/api/delete-subscription
     */
    apiControllerDeleteSubscription: (body: ObjectSubscription, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-subscription`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete syncer
     *
     * @tags Syncer API
     * @name ApiControllerDeleteSyncer
     * @request POST:/api/delete-syncer
     */
    apiControllerDeleteSyncer: (body: ObjectSyncer, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-syncer`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete token
     *
     * @tags Token API
     * @name ApiControllerDeleteToken
     * @request POST:/api/delete-token
     */
    apiControllerDeleteToken: (body: ObjectToken, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-token`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete user
     *
     * @tags User API
     * @name ApiControllerDeleteUser
     * @request POST:/api/delete-user
     */
    apiControllerDeleteUser: (body: ObjectUser, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-user`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description delete webhook
     *
     * @tags Webhook API
     * @name ApiControllerDeleteWebhook
     * @request POST:/api/delete-webhook
     */
    apiControllerDeleteWebhook: (body: ObjectWebhook, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/delete-webhook`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description Call Casbin Enforce API
     *
     * @tags Enforcer API
     * @name ApiControllerEnforce
     * @request POST:/api/enforce
     */
    apiControllerEnforce: (
      body: string[],
      query?: {
        /** permission id */
        permissionId?: string;
        /** model id */
        modelId?: string;
        /** resource id */
        resourceId?: string;
        /** owner */
        owner?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/enforce`,
        method: "POST",
        query: query,
        body: body,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description get the details of the current account
     *
     * @tags Account API
     * @name ApiControllerGetAccount
     * @request GET:/api/get-account
     */
    apiControllerGetAccount: (params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/get-account`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get adapter
     *
     * @tags Adapter API
     * @name ApiControllerGetAdapter
     * @request GET:/api/get-adapter
     */
    apiControllerGetAdapter: (
      query: {
        /** The id ( owner/name ) of the adapter */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectAdapter, any>({
        path: `/api/get-adapter`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get adapters
     *
     * @tags Adapter API
     * @name ApiControllerGetAdapters
     * @request GET:/api/get-adapters
     */
    apiControllerGetAdapters: (
      query: {
        /** The owner of adapters */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectAdapter[], any>({
        path: `/api/get-adapters`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get application login
     *
     * @tags Login API
     * @name ApiControllerGetApplicationLogin
     * @request GET:/api/get-app-login
     */
    apiControllerGetApplicationLogin: (
      query: {
        /** client id */
        clientId: string;
        /** response type */
        responseType: string;
        /** redirect uri */
        redirectUri: string;
        /** scope */
        scope: string;
        /** state */
        state: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/get-app-login`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get the detail of an application
     *
     * @tags Application API
     * @name ApiControllerGetApplication
     * @request GET:/api/get-application
     */
    apiControllerGetApplication: (
      query: {
        /** The id ( owner/name ) of the application. */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectApplication, any>({
        path: `/api/get-application`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get all applications
     *
     * @tags Application API
     * @name ApiControllerGetApplications
     * @request GET:/api/get-applications
     */
    apiControllerGetApplications: (
      query: {
        /** The owner of applications. */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectApplication[], any>({
        path: `/api/get-applications`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Login API
     * @name ApiControllerGetCaptcha
     * @request GET:/api/get-captcha
     */
    apiControllerGetCaptcha: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/get-captcha`,
        method: "GET",
        ...params,
      }),

    /**
     * @description Get Login Error Counts
     *
     * @tags Token API
     * @name ApiControllerGetCaptchaStatus
     * @request GET:/api/get-captcha-status
     */
    apiControllerGetCaptchaStatus: (
      query: {
        /** The id ( owner/name ) of user */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/get-captcha-status`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get cert
     *
     * @tags Cert API
     * @name ApiControllerGetCert
     * @request GET:/api/get-cert
     */
    apiControllerGetCert: (
      query: {
        /** The id ( owner/name ) of the cert */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectCert, any>({
        path: `/api/get-cert`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get certs
     *
     * @tags Cert API
     * @name ApiControllerGetCerts
     * @request GET:/api/get-certs
     */
    apiControllerGetCerts: (
      query: {
        /** The owner of certs */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectCert[], any>({
        path: `/api/get-certs`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get information of dashboard
     *
     * @tags System API
     * @name ApiControllerGetDashboard
     * @request GET:/api/get-dashboard
     */
    apiControllerGetDashboard: (params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/get-dashboard`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get default application
     *
     * @tags Organization API
     * @name ApiControllerGetDefaultApplication
     * @request GET:/api/get-default-application
     */
    apiControllerGetDefaultApplication: (
      query: {
        /** organization id */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/get-default-application`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get email and phone by username
     *
     * @tags User API
     * @name ApiControllerGetEmailAndPhone
     * @request GET:/api/get-email-and-phone
     */
    apiControllerGetEmailAndPhone: (
      data: {
        /** The username of the user */
        username: string;
        /** The organization of the user */
        organization: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/get-email-and-phone`,
        method: "GET",
        body: data,
        type: ContentType.UrlEncoded,
        ...params,
      }),

    /**
     * @description get enforcer
     *
     * @tags Enforcer API
     * @name ApiControllerGetEnforcer
     * @request GET:/api/get-enforcer
     */
    apiControllerGetEnforcer: (
      query: {
        /** The id ( owner/name )  of enforcer */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectEnforcer, any>({
        path: `/api/get-enforcer`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get enforcers
     *
     * @tags Enforcer API
     * @name ApiControllerGetEnforcers
     * @request GET:/api/get-enforcers
     */
    apiControllerGetEnforcers: (
      query: {
        /** The owner of enforcers */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectEnforcer[], any>({
        path: `/api/get-enforcers`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get globle certs
     *
     * @tags Cert API
     * @name ApiControllerGetGlobalCerts
     * @request GET:/api/get-global-certs
     */
    apiControllerGetGlobalCerts: (params: RequestParams = {}) =>
      this.request<ObjectCert[], any>({
        path: `/api/get-global-certs`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get Global providers
     *
     * @tags Provider API
     * @name ApiControllerGetGlobalProviders
     * @request GET:/api/get-global-providers
     */
    apiControllerGetGlobalProviders: (params: RequestParams = {}) =>
      this.request<ObjectProvider[], any>({
        path: `/api/get-global-providers`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get global users
     *
     * @tags User API
     * @name ApiControllerGetGlobalUsers
     * @request GET:/api/get-global-users
     */
    apiControllerGetGlobalUsers: (params: RequestParams = {}) =>
      this.request<ObjectUser[], any>({
        path: `/api/get-global-users`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get group
     *
     * @tags Group API
     * @name ApiControllerGetGroup
     * @request GET:/api/get-group
     */
    apiControllerGetGroup: (
      query: {
        /** The id ( owner/name ) of the group */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectGroup, any>({
        path: `/api/get-group`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get groups
     *
     * @tags Group API
     * @name ApiControllerGetGroups
     * @request GET:/api/get-groups
     */
    apiControllerGetGroups: (
      query: {
        /** The owner of groups */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectGroup[], any>({
        path: `/api/get-groups`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get invitation
     *
     * @tags Invitation API
     * @name ApiControllerGetInvitation
     * @request GET:/api/get-invitation
     */
    apiControllerGetInvitation: (
      query: {
        /** The id ( owner/name ) of the invitation */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectInvitation, any>({
        path: `/api/get-invitation`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get invitations
     *
     * @tags Invitation API
     * @name ApiControllerGetInvitations
     * @request GET:/api/get-invitations
     */
    apiControllerGetInvitations: (
      query: {
        /** The owner of invitations */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectInvitation[], any>({
        path: `/api/get-invitations`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get ldap
     *
     * @tags Account API
     * @name ApiControllerGetLdap
     * @request GET:/api/get-ldap
     */
    apiControllerGetLdap: (
      query: {
        /** id */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectLdap, any>({
        path: `/api/get-ldap`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get ldap users
     *
     * @tags Account API
     * @name ApiControllerGetLdapser
     * @request GET:/api/get-ldap-users
     */
    apiControllerGetLdapser: (params: RequestParams = {}) =>
      this.request<ControllersLdapResp, any>({
        path: `/api/get-ldap-users`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get ldaps
     *
     * @tags Account API
     * @name ApiControllerGetLdaps
     * @request GET:/api/get-ldaps
     */
    apiControllerGetLdaps: (
      query?: {
        /** owner */
        owner?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectLdap[], any>({
        path: `/api/get-ldaps`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get model
     *
     * @tags Model API
     * @name ApiControllerGetModel
     * @request GET:/api/get-model
     */
    apiControllerGetModel: (
      query: {
        /** The id ( owner/name ) of the model */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectModel, any>({
        path: `/api/get-model`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get models
     *
     * @tags Model API
     * @name ApiControllerGetModels
     * @request GET:/api/get-models
     */
    apiControllerGetModels: (
      query: {
        /** The owner of models */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectModel[], any>({
        path: `/api/get-models`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get organization
     *
     * @tags Organization API
     * @name ApiControllerGetOrganization
     * @request GET:/api/get-organization
     */
    apiControllerGetOrganization: (
      query: {
        /** organization id */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectOrganization, any>({
        path: `/api/get-organization`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get the detail of the organization's application
     *
     * @tags Application API
     * @name ApiControllerGetOrganizationApplications
     * @request GET:/api/get-organization-applications
     */
    apiControllerGetOrganizationApplications: (
      query: {
        /** The organization name */
        organization: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectApplication[], any>({
        path: `/api/get-organization-applications`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get all organization name and displayName
     *
     * @tags Organization API
     * @name ApiControllerGetOrganizationNames
     * @request GET:/api/get-organization-names
     */
    apiControllerGetOrganizationNames: (
      query: {
        /** owner */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectOrganization[], any>({
        path: `/api/get-organization-names`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get organizations
     *
     * @tags Organization API
     * @name ApiControllerGetOrganizations
     * @request GET:/api/get-organizations
     */
    apiControllerGetOrganizations: (
      query: {
        /** owner */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectOrganization[], any>({
        path: `/api/get-organizations`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get payment
     *
     * @tags Payment API
     * @name ApiControllerGetPayment
     * @request GET:/api/get-payment
     */
    apiControllerGetPayment: (
      query: {
        /** The id ( owner/name ) of the payment */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPayment, any>({
        path: `/api/get-payment`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get payments
     *
     * @tags Payment API
     * @name ApiControllerGetPayments
     * @request GET:/api/get-payments
     */
    apiControllerGetPayments: (
      query: {
        /** The owner of payments */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPayment[], any>({
        path: `/api/get-payments`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get permission
     *
     * @tags Permission API
     * @name ApiControllerGetPermission
     * @request GET:/api/get-permission
     */
    apiControllerGetPermission: (
      query: {
        /** The id ( owner/name ) of the permission */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPermission, any>({
        path: `/api/get-permission`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get permissions
     *
     * @tags Permission API
     * @name ApiControllerGetPermissions
     * @request GET:/api/get-permissions
     */
    apiControllerGetPermissions: (
      query: {
        /** The owner of permissions */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPermission[], any>({
        path: `/api/get-permissions`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get permissions by role
     *
     * @tags Permission API
     * @name ApiControllerGetPermissionsByRole
     * @request GET:/api/get-permissions-by-role
     */
    apiControllerGetPermissionsByRole: (
      query: {
        /** The id ( owner/name ) of the role */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPermission[], any>({
        path: `/api/get-permissions-by-role`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get permissions by submitter
     *
     * @tags Permission API
     * @name ApiControllerGetPermissionsBySubmitter
     * @request GET:/api/get-permissions-by-submitter
     */
    apiControllerGetPermissionsBySubmitter: (params: RequestParams = {}) =>
      this.request<ObjectPermission[], any>({
        path: `/api/get-permissions-by-submitter`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get plan
     *
     * @tags Plan API
     * @name ApiControllerGetPlan
     * @request GET:/api/get-plan
     */
    apiControllerGetPlan: (
      query: {
        /** The id ( owner/name ) of the plan */
        id: string;
        /** Should include plan's option */
        includeOption?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPlan, any>({
        path: `/api/get-plan`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get plans
     *
     * @tags Plan API
     * @name ApiControllerGetPlans
     * @request GET:/api/get-plans
     */
    apiControllerGetPlans: (
      query: {
        /** The owner of plans */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPlan[], any>({
        path: `/api/get-plans`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get pricing
     *
     * @tags Pricing API
     * @name ApiControllerGetPricing
     * @request GET:/api/get-pricing
     */
    apiControllerGetPricing: (
      query: {
        /** The id ( owner/name ) of the pricing */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPricing, any>({
        path: `/api/get-pricing`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get pricings
     *
     * @tags Pricing API
     * @name ApiControllerGetPricings
     * @request GET:/api/get-pricings
     */
    apiControllerGetPricings: (
      query: {
        /** The owner of pricings */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPricing[], any>({
        path: `/api/get-pricings`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get product
     *
     * @tags Product API
     * @name ApiControllerGetProduct
     * @request GET:/api/get-product
     */
    apiControllerGetProduct: (
      query: {
        /** The id ( owner/name ) of the product */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectProduct, any>({
        path: `/api/get-product`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get products
     *
     * @tags Product API
     * @name ApiControllerGetProducts
     * @request GET:/api/get-products
     */
    apiControllerGetProducts: (
      query: {
        /** The owner of products */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectProduct[], any>({
        path: `/api/get-products`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get Prometheus Info
     *
     * @tags System API
     * @name ApiControllerGetPrometheusInfo
     * @request GET:/api/get-prometheus-info
     */
    apiControllerGetPrometheusInfo: (params: RequestParams = {}) =>
      this.request<ObjectPrometheusInfo, any>({
        path: `/api/get-prometheus-info`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get provider
     *
     * @tags Provider API
     * @name ApiControllerGetProvider
     * @request GET:/api/get-provider
     */
    apiControllerGetProvider: (
      query: {
        /** The id ( owner/name ) of the provider */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectProvider, any>({
        path: `/api/get-provider`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get providers
     *
     * @tags Provider API
     * @name ApiControllerGetProviders
     * @request GET:/api/get-providers
     */
    apiControllerGetProviders: (
      query: {
        /** The owner of providers */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectProvider[], any>({
        path: `/api/get-providers`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get resource
     *
     * @tags Resource API
     * @name ApiControllerGetResource
     * @request GET:/api/get-resource
     */
    apiControllerGetResource: (
      query: {
        /** The id ( owner/name ) of resource */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectResource, any>({
        path: `/api/get-resource`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get resources
     *
     * @tags Resource API
     * @name ApiControllerGetResources
     * @request GET:/api/get-resources
     */
    apiControllerGetResources: (
      query: {
        /** Owner */
        owner: string;
        /** User */
        user: string;
        /** Page Size */
        pageSize?: number;
        /** Page Number */
        p?: number;
        /** Field */
        field?: string;
        /** Value */
        value?: string;
        /** Sort Field */
        sortField?: string;
        /** Sort Order */
        sortOrder?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectResource[], any>({
        path: `/api/get-resources`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get role
     *
     * @tags Role API
     * @name ApiControllerGetRole
     * @request GET:/api/get-role
     */
    apiControllerGetRole: (
      query: {
        /** The id ( owner/name ) of the role */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectRole, any>({
        path: `/api/get-role`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get roles
     *
     * @tags Role API
     * @name ApiControllerGetRoles
     * @request GET:/api/get-roles
     */
    apiControllerGetRoles: (
      query: {
        /** The owner of roles */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectRole[], any>({
        path: `/api/get-roles`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description Get session for one user in one application.
     *
     * @tags Session API
     * @name ApiControllerGetSingleSession
     * @request GET:/api/get-session
     */
    apiControllerGetSingleSession: (
      query: {
        /** The id(organization/application/user) of session */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string[], any>({
        path: `/api/get-session`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description Get organization user sessions.
     *
     * @tags Session API
     * @name ApiControllerGetSessions
     * @request GET:/api/get-sessions
     */
    apiControllerGetSessions: (
      query: {
        /** The organization name */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string[], any>({
        path: `/api/get-sessions`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User API
     * @name ApiControllerGetSortedUsers
     * @request GET:/api/get-sorted-users
     */
    apiControllerGetSortedUsers: (
      query: {
        /** The owner of users */
        owner: string;
        /** The DB column name to sort by, e.g., created_time */
        sorter: string;
        /** The count of users to return, e.g., 25 */
        limit: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectUser[], any>({
        path: `/api/get-sorted-users`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get subscription
     *
     * @tags Subscription API
     * @name ApiControllerGetSubscription
     * @request GET:/api/get-subscription
     */
    apiControllerGetSubscription: (
      query: {
        /** The id ( owner/name ) of the subscription */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectSubscription, any>({
        path: `/api/get-subscription`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get subscriptions
     *
     * @tags Subscription API
     * @name ApiControllerGetSubscriptions
     * @request GET:/api/get-subscriptions
     */
    apiControllerGetSubscriptions: (
      query: {
        /** The owner of subscriptions */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectSubscription[], any>({
        path: `/api/get-subscriptions`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get syncer
     *
     * @tags Syncer API
     * @name ApiControllerGetSyncer
     * @request GET:/api/get-syncer
     */
    apiControllerGetSyncer: (
      query: {
        /** The id ( owner/name ) of the syncer */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectSyncer, any>({
        path: `/api/get-syncer`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get syncers
     *
     * @tags Syncer API
     * @name ApiControllerGetSyncers
     * @request GET:/api/get-syncers
     */
    apiControllerGetSyncers: (
      query: {
        /** The owner of syncers */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectSyncer[], any>({
        path: `/api/get-syncers`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get system info like CPU and memory usage
     *
     * @tags System API
     * @name ApiControllerGetSystemInfo
     * @request GET:/api/get-system-info
     */
    apiControllerGetSystemInfo: (params: RequestParams = {}) =>
      this.request<UtilSystemInfo, any>({
        path: `/api/get-system-info`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get token
     *
     * @tags Token API
     * @name ApiControllerGetToken
     * @request GET:/api/get-token
     */
    apiControllerGetToken: (
      query: {
        /** The id ( owner/name ) of token */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectToken, any>({
        path: `/api/get-token`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get tokens
     *
     * @tags Token API
     * @name ApiControllerGetTokens
     * @request GET:/api/get-tokens
     */
    apiControllerGetTokens: (
      query: {
        /** The owner of tokens */
        owner: string;
        /** The size of each page */
        pageSize: string;
        /** The number of the page */
        p: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectToken[], any>({
        path: `/api/get-tokens`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get user
     *
     * @tags User API
     * @name ApiControllerGetUser
     * @request GET:/api/get-user
     */
    apiControllerGetUser: (
      query?: {
        /** The id ( owner/name ) of the user */
        id?: string;
        /** The owner of the user */
        owner?: string;
        /** The email of the user */
        email?: string;
        /** The phone of the user */
        phone?: string;
        /** The userId of the user */
        userId?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectUser, any>({
        path: `/api/get-user`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get the detail of the user's application
     *
     * @tags Application API
     * @name ApiControllerGetUserApplication
     * @request GET:/api/get-user-application
     */
    apiControllerGetUserApplication: (
      query: {
        /** The id ( owner/name ) of the user */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectApplication, any>({
        path: `/api/get-user-application`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User API
     * @name ApiControllerGetUserCount
     * @request GET:/api/get-user-count
     */
    apiControllerGetUserCount: (
      query: {
        /** The owner of users */
        owner: string;
        /** The filter for query, 1 for online, 0 for offline, empty string for all users */
        isOnline: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/api/get-user-count`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get payments for a user
     *
     * @tags Payment API
     * @name ApiControllerGetUserPayments
     * @request GET:/api/get-user-payments
     */
    apiControllerGetUserPayments: (
      query: {
        /** The owner of payments */
        owner: string;
        /** The organization of the user */
        organization: string;
        /** The username of the user */
        user: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectPayment[], any>({
        path: `/api/get-user-payments`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User API
     * @name ApiControllerGetUsers
     * @request GET:/api/get-users
     */
    apiControllerGetUsers: (
      query: {
        /** The owner of users */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectUser[], any>({
        path: `/api/get-users`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description get version info like Casdoor release version and commit ID
     *
     * @tags System API
     * @name ApiControllerGetVersionInfo
     * @request GET:/api/get-version-info
     */
    apiControllerGetVersionInfo: (params: RequestParams = {}) =>
      this.request<UtilVersionInfo, any>({
        path: `/api/get-version-info`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get webhook
     *
     * @tags Webhook API
     * @name ApiControllerGetWebhook
     * @request GET:/api/get-webhook
     */
    apiControllerGetWebhook: (
      query: {
        /**
         * The id ( owner/name ) of the webhook
         * @default "built-in/admin"
         */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectWebhook, any>({
        path: `/api/get-webhook`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags System API
     * @name ApiControllerGetWebhookEventType
     * @request GET:/api/get-webhook-event
     */
    apiControllerGetWebhookEventType: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/get-webhook-event`,
        method: "GET",
        ...params,
      }),

    /**
     * @description get webhooks
     *
     * @tags Webhook API
     * @name ApiControllerGetWebhooks
     * @request GET:/api/get-webhooks
     * @secure
     */
    apiControllerGetWebhooks: (
      query: {
        /**
         * The owner of webhooks
         * @default "built-in/admin"
         */
        owner: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectWebhook[], any>({
        path: `/api/get-webhooks`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description check if the system is live
     *
     * @tags System API
     * @name ApiControllerHealth
     * @request GET:/api/health
     */
    apiControllerHealth: (params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/health`,
        method: "GET",
        ...params,
      }),

    /**
     * @description invoice payment
     *
     * @tags Payment API
     * @name ApiControllerInvoicePayment
     * @request POST:/api/invoice-payment
     */
    apiControllerInvoicePayment: (
      query: {
        /** The id ( owner/name ) of the payment */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/invoice-payment`,
        method: "POST",
        query: query,
        ...params,
      }),

    /**
     * @description Check if there are other different sessions for one user in one application.
     *
     * @tags Session API
     * @name ApiControllerIsSessionDuplicated
     * @request GET:/api/is-session-duplicated
     */
    apiControllerIsSessionDuplicated: (
      query: {
        /** The id(organization/application/user) of session */
        id: string;
        /** sessionId to be checked */
        sessionId: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string[], any>({
        path: `/api/is-session-duplicated`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description login
     *
     * @tags Login API
     * @name ApiControllerLogin
     * @request POST:/api/login
     */
    apiControllerLogin: (
      query: {
        /** clientId */
        clientId: string;
        /** responseType */
        responseType: string;
        /** redirectUri */
        redirectUri: string;
        /** scope */
        scope?: string;
        /** state */
        state?: string;
        /** nonce */
        nonce?: string;
        /** code_challenge_method */
        code_challenge_method?: string;
        /** code_challenge */
        code_challenge?: string;
      },
      form: ControllersAuthForm,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/login`,
        method: "POST",
        query: query,
        body: form,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description get OAuth access token
     *
     * @tags Token API
     * @name ApiControllerGetOAuthToken
     * @request POST:/api/login/oauth/access_token
     */
    apiControllerGetOAuthToken: (
      query: {
        /** OAuth grant type */
        grant_type: string;
        /** OAuth client id */
        client_id: string;
        /** OAuth client secret */
        client_secret: string;
        /** OAuth code */
        code: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectTokenWrapper, ObjectTokenError>({
        path: `/api/login/oauth/access_token`,
        method: "POST",
        query: query,
        ...params,
      }),

    /**
     * @description The introspection endpoint is an OAuth 2.0 endpoint that takes a
     *
     * @tags Login API
     * @name ApiControllerIntrospectToken
     * @request POST:/api/login/oauth/introspect
     */
    apiControllerIntrospectToken: (
      data: {
        /** access_token's value or refresh_token's value */
        token: string;
        /** the token type access_token or refresh_token */
        token_type_hint: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectIntrospectionResponse, ObjectTokenError>({
        path: `/api/login/oauth/introspect`,
        method: "POST",
        body: data,
        type: ContentType.UrlEncoded,
        ...params,
      }),

    /**
     * @description refresh OAuth access token
     *
     * @tags Token API
     * @name ApiControllerRefreshToken
     * @request POST:/api/login/oauth/refresh_token
     */
    apiControllerRefreshToken: (
      query: {
        /** OAuth grant type */
        grant_type: string;
        /** OAuth refresh token */
        refresh_token: string;
        /** OAuth scope */
        scope: string;
        /** OAuth client id */
        client_id: string;
        /** OAuth client secret */
        client_secret?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectTokenWrapper, ObjectTokenError>({
        path: `/api/login/oauth/refresh_token`,
        method: "POST",
        query: query,
        ...params,
      }),

    /**
     * @description logout the current user
     *
     * @tags Login API
     * @name ApiControllerLogout
     * @request POST:/api/logout
     */
    apiControllerLogout: (
      query?: {
        /** id_token_hint */
        id_token_hint?: string;
        /** post_logout_redirect_uri */
        post_logout_redirect_uri?: string;
        /** state */
        state?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/logout`,
        method: "POST",
        query: query,
        ...params,
      }),

    /**
     * @description enable totp
     *
     * @tags MFA API
     * @name ApiControllerMfaSetupEnable
     * @request POST:/api/mfa/setup/enable
     */
    apiControllerMfaSetupEnable: (params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/mfa/setup/enable`,
        method: "POST",
        ...params,
      }),

    /**
     * @description setup MFA
     *
     * @tags MFA API
     * @name ApiControllerMfaSetupInitiate
     * @request POST:/api/mfa/setup/initiate
     */
    apiControllerMfaSetupInitiate: (params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/mfa/setup/initiate`,
        method: "POST",
        ...params,
      }),

    /**
     * @description setup verify totp
     *
     * @tags MFA API
     * @name ApiControllerMfaSetupVerify
     * @request POST:/api/mfa/setup/verify
     */
    apiControllerMfaSetupVerify: (params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/mfa/setup/verify`,
        method: "POST",
        ...params,
      }),

    /**
     * @description notify payment
     *
     * @tags Payment API
     * @name ApiControllerNotifyPayment
     * @request POST:/api/notify-payment
     */
    apiControllerNotifyPayment: (body: ObjectPayment, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/notify-payment`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Account API
     * @name ApiControllerResetEmailOrPhone
     * @request POST:/api/reset-email-or-phone
     */
    apiControllerResetEmailOrPhone: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/reset-email-or-phone`,
        method: "POST",
        ...params,
      }),

    /**
     * @description run syncer
     *
     * @tags Syncer API
     * @name ApiControllerRunSyncer
     * @request GET:/api/run-syncer
     */
    apiControllerRunSyncer: (body: ObjectSyncer, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/run-syncer`,
        method: "GET",
        body: body,
        ...params,
      }),

    /**
     * @description This API is not for Casdoor frontend to call, it is for Casdoor SDKs.
     *
     * @tags Service API
     * @name ApiControllerSendEmail
     * @request POST:/api/send-email
     */
    apiControllerSendEmail: (
      query: {
        /** The clientId of the application */
        clientId: string;
        /** The clientSecret of the application */
        clientSecret: string;
      },
      from: ControllersEmailForm,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/send-email`,
        method: "POST",
        query: query,
        body: from,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description This API is not for Casdoor frontend to call, it is for Casdoor SDKs.
     *
     * @tags Service API
     * @name ApiControllerSendNotification
     * @request POST:/api/send-notification
     */
    apiControllerSendNotification: (from: ControllersNotificationForm, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/send-notification`,
        method: "POST",
        body: from,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description This API is not for Casdoor frontend to call, it is for Casdoor SDKs.
     *
     * @tags Service API
     * @name ApiControllerSendSms
     * @request POST:/api/send-sms
     */
    apiControllerSendSms: (
      query: {
        /** The clientId of the application */
        clientId: string;
        /** The clientSecret of the application */
        clientSecret: string;
      },
      from: ControllersSmsForm,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/send-sms`,
        method: "POST",
        query: query,
        body: from,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Verification API
     * @name ApiControllerSendVerificationCode
     * @request POST:/api/send-verification-code
     */
    apiControllerSendVerificationCode: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/send-verification-code`,
        method: "POST",
        ...params,
      }),

    /**
     * @description set password
     *
     * @tags Account API
     * @name ApiControllerSetPassword
     * @request POST:/api/set-password
     */
    apiControllerSetPassword: (
      data: {
        /** The owner of the user */
        userOwner: string;
        /** The name of the user */
        userName: string;
        /** The old password of the user */
        oldPassword: string;
        /** The new password of the user */
        newPassword: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/set-password`,
        method: "POST",
        body: data,
        type: ContentType.UrlEncoded,
        ...params,
      }),

    /**
     * @description : Set specific Mfa Preferred
     *
     * @tags MFA API
     * @name ApiControllerSetPreferredMfa
     * @request POST:/api/set-preferred-mfa
     */
    apiControllerSetPreferredMfa: (params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/set-preferred-mfa`,
        method: "POST",
        ...params,
      }),

    /**
     * @description sign up a new user
     *
     * @tags Login API
     * @name ApiControllerSignup
     * @request POST:/api/signup
     */
    apiControllerSignup: (
      data: {
        /** The username to sign up */
        username: string;
        /** The password */
        password: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/signup`,
        method: "POST",
        body: data,
        type: ContentType.UrlEncoded,
        ...params,
      }),

    /**
     * @description sync ldap users
     *
     * @tags Account API
     * @name ApiControllerSyncLdapUsers
     * @request POST:/api/sync-ldap-users
     */
    apiControllerSyncLdapUsers: (
      query: {
        /** id */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersLdapSyncResp, any>({
        path: `/api/sync-ldap-users`,
        method: "POST",
        query: query,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Login API
     * @name ApiControllerUnlink
     * @request POST:/api/unlink
     */
    apiControllerUnlink: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/unlink`,
        method: "POST",
        ...params,
      }),

    /**
     * @description update adapter
     *
     * @tags Adapter API
     * @name ApiControllerUpdateAdapter
     * @request POST:/api/update-adapter
     */
    apiControllerUpdateAdapter: (
      query: {
        /** The id ( owner/name ) of the adapter */
        id: string;
      },
      body: ObjectAdapter,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-adapter`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update an application
     *
     * @tags Application API
     * @name ApiControllerUpdateApplication
     * @request POST:/api/update-application
     */
    apiControllerUpdateApplication: (
      query: {
        /** The id ( owner/name ) of the application */
        id: string;
      },
      body: ObjectApplication,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-application`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update cert
     *
     * @tags Cert API
     * @name ApiControllerUpdateCert
     * @request POST:/api/update-cert
     */
    apiControllerUpdateCert: (
      query: {
        /** The id ( owner/name ) of the cert */
        id: string;
      },
      body: ObjectCert,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-cert`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update enforcer
     *
     * @tags Enforcer API
     * @name ApiControllerUpdateEnforcer
     * @request POST:/api/update-enforcer
     */
    apiControllerUpdateEnforcer: (
      query: {
        /** The id ( owner/name )  of enforcer */
        id: string;
      },
      enforcer: Object,
      params: RequestParams = {},
    ) =>
      this.request<ObjectEnforcer, any>({
        path: `/api/update-enforcer`,
        method: "POST",
        query: query,
        body: enforcer,
        ...params,
      }),

    /**
     * @description update group
     *
     * @tags Group API
     * @name ApiControllerUpdateGroup
     * @request POST:/api/update-group
     */
    apiControllerUpdateGroup: (
      query: {
        /** The id ( owner/name ) of the group */
        id: string;
      },
      body: ObjectGroup,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-group`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update invitation
     *
     * @tags Invitation API
     * @name ApiControllerUpdateInvitation
     * @request POST:/api/update-invitation
     */
    apiControllerUpdateInvitation: (
      query: {
        /** The id ( owner/name ) of the invitation */
        id: string;
      },
      body: ObjectInvitation,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-invitation`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update ldap
     *
     * @tags Account API
     * @name ApiControllerUpdateLdap
     * @request POST:/api/update-ldap
     */
    apiControllerUpdateLdap: (body: ObjectLdap, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-ldap`,
        method: "POST",
        body: body,
        ...params,
      }),

    /**
     * @description update model
     *
     * @tags Model API
     * @name ApiControllerUpdateModel
     * @request POST:/api/update-model
     */
    apiControllerUpdateModel: (
      query: {
        /** The id ( owner/name ) of the model */
        id: string;
      },
      body: ObjectModel,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-model`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update organization
     *
     * @tags Organization API
     * @name ApiControllerUpdateOrganization
     * @request POST:/api/update-organization
     */
    apiControllerUpdateOrganization: (
      query: {
        /** The id ( owner/name ) of the organization */
        id: string;
      },
      body: ObjectOrganization,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-organization`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update payment
     *
     * @tags Payment API
     * @name ApiControllerUpdatePayment
     * @request POST:/api/update-payment
     */
    apiControllerUpdatePayment: (
      query: {
        /** The id ( owner/name ) of the payment */
        id: string;
      },
      body: ObjectPayment,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-payment`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update permission
     *
     * @tags Permission API
     * @name ApiControllerUpdatePermission
     * @request POST:/api/update-permission
     */
    apiControllerUpdatePermission: (
      query: {
        /** The id ( owner/name ) of the permission */
        id: string;
      },
      body: ObjectPermission,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-permission`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update plan
     *
     * @tags Plan API
     * @name ApiControllerUpdatePlan
     * @request POST:/api/update-plan
     */
    apiControllerUpdatePlan: (
      query: {
        /** The id ( owner/name ) of the plan */
        id: string;
      },
      body: ObjectPlan,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-plan`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update pricing
     *
     * @tags Pricing API
     * @name ApiControllerUpdatePricing
     * @request POST:/api/update-pricing
     */
    apiControllerUpdatePricing: (
      query: {
        /** The id ( owner/name ) of the pricing */
        id: string;
      },
      body: ObjectPricing,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-pricing`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update product
     *
     * @tags Product API
     * @name ApiControllerUpdateProduct
     * @request POST:/api/update-product
     */
    apiControllerUpdateProduct: (
      query: {
        /** The id ( owner/name ) of the product */
        id: string;
      },
      body: ObjectProduct,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-product`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update provider
     *
     * @tags Provider API
     * @name ApiControllerUpdateProvider
     * @request POST:/api/update-provider
     */
    apiControllerUpdateProvider: (
      query: {
        /** The id ( owner/name ) of the provider */
        id: string;
      },
      body: ObjectProvider,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-provider`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description get resource
     *
     * @tags Resource API
     * @name ApiControllerUpdateResource
     * @request POST:/api/update-resource
     */
    apiControllerUpdateResource: (
      query: {
        /** The id ( owner/name ) of resource */
        id: string;
      },
      resource: ObjectResource,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-resource`,
        method: "POST",
        query: query,
        body: resource,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description update role
     *
     * @tags Role API
     * @name ApiControllerUpdateRole
     * @request POST:/api/update-role
     */
    apiControllerUpdateRole: (
      query: {
        /** The id ( owner/name ) of the role */
        id: string;
      },
      body: ObjectRole,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-role`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description Update session for one user in one application.
     *
     * @tags Session API
     * @name ApiControllerUpdateSession
     * @request POST:/api/update-session
     */
    apiControllerUpdateSession: (
      query: {
        /** The id(organization/application/user) of session */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string[], any>({
        path: `/api/update-session`,
        method: "POST",
        query: query,
        ...params,
      }),

    /**
     * @description update subscription
     *
     * @tags Subscription API
     * @name ApiControllerUpdateSubscription
     * @request POST:/api/update-subscription
     */
    apiControllerUpdateSubscription: (
      query: {
        /** The id ( owner/name ) of the subscription */
        id: string;
      },
      body: ObjectSubscription,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-subscription`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update syncer
     *
     * @tags Syncer API
     * @name ApiControllerUpdateSyncer
     * @request POST:/api/update-syncer
     */
    apiControllerUpdateSyncer: (
      query: {
        /** The id ( owner/name ) of the syncer */
        id: string;
      },
      body: ObjectSyncer,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-syncer`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update token
     *
     * @tags Token API
     * @name ApiControllerUpdateToken
     * @request POST:/api/update-token
     */
    apiControllerUpdateToken: (
      query: {
        /** The id ( owner/name ) of token */
        id: string;
      },
      body: ObjectToken,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-token`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update user
     *
     * @tags User API
     * @name ApiControllerUpdateUser
     * @request POST:/api/update-user
     */
    apiControllerUpdateUser: (
      query: {
        /** The id ( owner/name ) of the user */
        id: string;
      },
      body: ObjectUser,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-user`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * @description update webhook
     *
     * @tags Webhook API
     * @name ApiControllerUpdateWebhook
     * @request POST:/api/update-webhook
     */
    apiControllerUpdateWebhook: (
      query: {
        /**
         * The id ( owner/name ) of the webhook
         * @default "built-in/admin"
         */
        id: string;
      },
      body: ObjectWebhook,
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/update-webhook`,
        method: "POST",
        query: query,
        body: body,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Resource API
     * @name ApiControllerUploadResource
     * @request POST:/api/upload-resource
     */
    apiControllerUploadResource: (
      query: {
        /** Owner */
        owner: string;
        /** User */
        user: string;
        /** Application */
        application: string;
        /** Tag */
        tag?: string;
        /** Parent */
        parent?: string;
        /** Full File Path */
        fullFilePath: string;
        /** Created Time */
        createdTime?: string;
        /** Description */
        description?: string;
      },
      data: {
        /** Resource file */
        file: File;
      },
      params: RequestParams = {},
    ) =>
      this.request<ObjectResource, any>({
        path: `/api/upload-resource`,
        method: "POST",
        query: query,
        body: data,
        type: ContentType.FormData,
        ...params,
      }),

    /**
     * @description return Laravel compatible user information according to OAuth 2.0
     *
     * @tags Account API
     * @name ApiControllerUserInfo2
     * @request GET:/api/user
     */
    apiControllerUserInfo2: (params: RequestParams = {}) =>
      this.request<ControllersLaravelResponse, any>({
        path: `/api/user`,
        method: "GET",
        ...params,
      }),

    /**
     * @description return user information according to OIDC standards
     *
     * @tags Account API
     * @name ApiControllerUserInfo
     * @request GET:/api/userinfo
     */
    apiControllerUserInfo: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/userinfo`,
        method: "GET",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Verification API
     * @name ApiControllerVerifyCaptcha
     * @request POST:/api/verify-captcha
     */
    apiControllerVerifyCaptcha: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/verify-captcha`,
        method: "POST",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Verification API
     * @name ApiControllerVerifyCode
     * @request POST:/api/verify-code
     */
    apiControllerVerifyCode: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/verify-code`,
        method: "POST",
        ...params,
      }),

    /**
     * @description verify invitation
     *
     * @tags Invitation API
     * @name ApiControllerVerifyInvitation
     * @request GET:/api/verify-invitation
     */
    apiControllerVerifyInvitation: (
      query: {
        /** The id ( owner/name ) of the invitation */
        id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ControllersResponse, any>({
        path: `/api/verify-invitation`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description WebAuthn Login Flow 1st stage
     *
     * @tags Login API
     * @name ApiControllerWebAuthnSigninBegin
     * @request GET:/api/webauthn/signin/begin
     */
    apiControllerWebAuthnSigninBegin: (
      query: {
        /** owner */
        owner: string;
        /** name */
        name: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ProtocolCredentialAssertion, any>({
        path: `/api/webauthn/signin/begin`,
        method: "GET",
        query: query,
        ...params,
      }),

    /**
     * @description WebAuthn Login Flow 2nd stage
     *
     * @tags Login API
     * @name ApiControllerWebAuthnSigninFinish
     * @request POST:/api/webauthn/signin/finish
     */
    apiControllerWebAuthnSigninFinish: (body: ProtocolCredentialAssertionResponse, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/webauthn/signin/finish`,
        method: "POST",
        body: body,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description WebAuthn Registration Flow 1st stage
     *
     * @tags User API
     * @name ApiControllerWebAuthnSignupBegin
     * @request GET:/api/webauthn/signup/begin
     */
    apiControllerWebAuthnSignupBegin: (params: RequestParams = {}) =>
      this.request<ProtocolCredentialCreation, any>({
        path: `/api/webauthn/signup/begin`,
        method: "GET",
        ...params,
      }),

    /**
     * @description WebAuthn Registration Flow 2nd stage
     *
     * @tags User API
     * @name ApiControllerWebAuthnSignupFinish
     * @request POST:/api/webauthn/signup/finish
     */
    apiControllerWebAuthnSignupFinish: (body: ProtocolCredentialCreationResponse, params: RequestParams = {}) =>
      this.request<ControllersResponse, any>({
        path: `/api/webauthn/signup/finish`,
        method: "POST",
        body: body,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * No description
     *
     * @tags System API
     * @name ApiControllerHandleOfficialAccountEvent
     * @request POST:/api/webhook
     */
    apiControllerHandleOfficialAccountEvent: (params: RequestParams = {}) =>
      this.request<ObjectUserinfo, any>({
        path: `/api/webhook`,
        method: "POST",
        ...params,
      }),
  };
}
