module.exports = {
    env: {
        node: true,
        browser: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:vue/vue3-recommended',
        '@vue/typescript/recommended',
        'prettier'
    ],
    plugins: [
        'prettier',
        '@typescript-eslint'
    ],
    rules: {
        "no-console": "warn",
        "no-unused-vars": "warn",
        "no-const-assign": "error",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-explicit-any": "warn",
        "@typescript-eslint/no-unused-vars": ["warn", { "argsIgnorePattern": "^_" }],
        "vue/html-closing-bracket-newline": ["error", { "multiline": "always" }],
        "prettier/prettier": "error",
    }
};
