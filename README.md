# Projet de création Test & Qualité

Ce projet a pour but de créer une application web permettant de gérer des utilisateurs en utilisant CasDoor comme système d'authentification.

## Prérequis

### Frontend

- [Docker](https://www.docker.com/)

### Backend

- [Node JS](https://nodejs.org/en/)

## Installation
Créer un network docker :
```bash
docker network create test-network
```

Démarer le serveur CasDoor avec docker-compose :
```bash
docker-compose up -d
```

Installer les dépendances du frontend et du backend :
```bash
npm install
```
Démarrer le serveur backend :
```bash
npm run backend
```
Démarrer le serveur frontend dans un autre terminal:
```bash
npm run dev
```
Et voilà ! Vous pouvez maintenant accéder à l'application sur [localhost:5173](http://localhost:5173/).

## Contributeurs

- [Lazare Fortune](https://lazarefortune.com/)
- [Arthur Iouzalen](https://gitlab.com/sroyart)
- [Maxime Grodet](https://gitlab.com/grodet95)
- [Maroine Zouaoui](https://gitlab.com/momo91)