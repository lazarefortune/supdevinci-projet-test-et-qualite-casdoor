/// <reference types="cypress" />

describe("Login", () => {
  it("should be accessible", () => {
    cy.visit("http://localhost:5173");
  });

  it("should say you are not logged in", () => {
    cy.visit("http://localhost:5173");
    cy.get("#title").should("contain", "Vous n'êtes pas connecté");
  });

  it("should let you login", () => {
    cy.login("admin", "123");
  });

  it("should display the users button", () => {
    cy.visit("http://localhost:5173");
    cy.get("#users").should("not.exist");
    cy.login("admin", "123");
    cy.get("#users").should("be.visible");
  });

  it("should logout", () => {
    cy.visit("http://localhost:5173");
    cy.get("#logout").should("not.exist");
    cy.login("admin", "123");
    cy.get("#logout").should("be.visible");
    cy.get("#logout").click();
    cy.url().should("eq", "http://localhost:5173/");
    cy.get("#logout").should("not.exist");
  });
});
