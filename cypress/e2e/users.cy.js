/// <reference types="cypress" />

describe("Users", () => {
  it("should be accessible", () => {
    cy.visit("http://localhost:5173/users");
  });

  it("should require login", () => {
    cy.visit("http://localhost:5173/users");
    cy.get("h2").should("contain", "You are not logged in");
    cy.get("#permissions-list").should("not.exist");
    cy.get("#add-user-button").should("not.exist");
    cy.get("#users-table").should("not.exist");
    cy.login("admin", "123");
    cy.get("#users").should("be.visible").click();
    cy.url().should("eq", "http://localhost:5173/users");
    cy.get("h2").should("contain", "You are");
    cy.get("#permissions-list").should("be.visible");
    cy.get("#permissions-list").should("contain", "Read");
    cy.get("#permissions-list").should("contain", "Write");
    cy.get("#add-user-button").should("be.visible");
    cy.get("#users-table").should("be.visible");
  });
});
